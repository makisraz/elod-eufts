package json.stats;

import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Literal;

import json.HelperMethods;
import json.QueryConfiguration;

import virtuoso.jena.driver.VirtGraph;
import virtuoso.jena.driver.VirtuosoQueryExecution;
import virtuoso.jena.driver.VirtuosoQueryExecutionFactory;

/**
 * @author G. Razis
 */
public class LargestsContracts {
	
	private static HelperMethods hm = new HelperMethods();

	public static void main(String[] args) {
		
		/* EuFts Graph */
		VirtGraph graphEuFts = new VirtGraph(QueryConfiguration.queryGraphEuFts, QueryConfiguration.connectionString, 
											 QueryConfiguration.username, QueryConfiguration.password);
		System.out.println("Connected to EuFts Graph!");
		
		/* Organizations Graph */
		VirtGraph graphOrgs = new VirtGraph(QueryConfiguration.queryGraphEuFts, QueryConfiguration.connectionString, 
											QueryConfiguration.username, QueryConfiguration.password);
		System.out.println("Connected to Organizations Graph!");

		/* initialize the JSON */
		String jsonString = "";

		/* start the 'couchDb' JSON */
		if (QueryConfiguration.couchDbUsage) {
			jsonString += "{\"docs\":";
		}

		/* start the 'normal' JSON */
		jsonString += "[{";

		/** create _id tag **/
		if (QueryConfiguration.couchDbUsage) {
			jsonString += "\"_id\": \"" + "eufts-largestsContracts" + "\",";
		}
		
		jsonString += "\"largestsContracts\": [";
		
		jsonString = getLargsetContracts(graphEuFts, graphOrgs, jsonString);
		
		jsonString += "]";
		
		/* close the 'normal' JSON */
		jsonString += "}]";

		/* close the 'couchDb' JSON */
		if (QueryConfiguration.couchDbUsage) {
			jsonString += "}";
		}

		hm.writeJsonFile(jsonString, "others", "largestsContracts");	
		
		graphOrgs.close();
		graphEuFts.close();
		
		System.out.println("\nFinished!");
	}
	
	private static String getLargsetContracts(VirtGraph graphEuFts, VirtGraph graphOrgs, String jsonString) {
		
		String queryLarge = "PREFIX elod: <http://linkedeconomy.org/ontology#> " +
				"PREFIX dcterms: <http://purl.org/dc/terms/>" +
				"PREFIX gr: <http://purl.org/goodrelations/v1#> " +
				"PREFIX skos: <http://www.w3.org/2004/02/skos/core#> " +
				"SELECT ?contract ?contractId ?buyer ?buyerName ?date (xsd:decimal(?amountTotal) AS ?totalAmount) " +
					   "(SAMPLE(?budgetLineLbl) AS ?budgetLineLbl) (SAMPLE(?fundingTypeLbl) AS ?fundingTypeLbl) (SAMPLE(?actionTypeLbl) AS ?actionTypeLbl) " +
				"FROM <" + QueryConfiguration.queryGraphEuFts + ">" +
				"WHERE { " +
				"?spendingItem elod:hasExpenditureLine ?expLine ; " +
							  "elod:hasRelatedContract ?contract . " +
				"?contract elod:buyer ?buyer ; " +
						  "elod:contractId ?contractId ; " +
						  "dcterms:issued ?date ; " +
						  "pc:actualPrice ?upsTotal . " +
				"?upsTotal gr:hasCurrencyValue ?amountTotal . " +
				"?buyer gr:legalName ?buyerName . " +
				"OPTIONAL { " +
					"?contract elod:hasFundingType ?fundingType . " +
					"?fundingType skos:prefLabel ?fundingTypeLbl . " +
				"} . " +
				"OPTIONAL { " +
					"?contract elod:hasActionType ?actionType . " +
					"?actionType skos:prefLabel ?actionTypeLbl . " +
				"} . " +
				"OPTIONAL { " +
					"?contract elod:hasBudgetLine ?budgetLine . " +
					"?budgetLine skos:prefLabel ?budgetLineLbl . " +
				"} . " +
				"FILTER (LANG(?fundingTypeLbl) = \"en\") . " +
				"FILTER (LANG(?actionTypeLbl) = \"en\") . " +
				"FILTER (LANG(?budgetLineLbl) = \"en\") . " +
				"} " +
				"ORDER BY DESC (?amountTotal) " +
				"LIMIT 1000";

		VirtuosoQueryExecution vqeLarge = VirtuosoQueryExecutionFactory.create(queryLarge, graphEuFts);
		ResultSet resultsLarge = vqeLarge.execSelect();

		while (resultsLarge.hasNext()) {
			QuerySolution result = resultsLarge.nextSolution();
			String buyerUri = result.getResource("buyer").getURI();
			String buyerName = result.getLiteral("buyerName").getString();
			String contractUri = result.getResource("contract").getURI();
			String contractId = result.getLiteral("contractId").getString();
			String date = result.getLiteral("date").getString().split("-")[0];
			Literal amountLit = result.getLiteral("totalAmount");
			String amount = hm.convertAmount(amountLit);
			String budgetLine = result.getLiteral("budgetLineLbl").getString();
			String fundingType = result.getLiteral("fundingTypeLbl").getString();
			String actionType = result.getLiteral("actionTypeLbl").getString();
			
			String buyerPart = "<a href=\\\"" + hm.urlProfilePage(buyerUri, "Buyer") + 
							   "\\\" target=\\\"_blank\\\">" + buyerName + "</a>";
			
			/* find the Sellers of each Contract */
			String sellerPart = getContractSellers(graphEuFts, contractUri);
			
			jsonString += "{\"contract\": \"" + contractId + "\"," + "\"buyer\": \"" + buyerPart + "\"," + 
						  "\"seller\": \"" + sellerPart + "\"," + "\"date\": \"" + date + "\"," + "\"amount\": \"" + 
						  amount + "\"," + "\"budgetLine\": \"" + budgetLine + "\"," + "\"fundingType\": \"" + 
						  fundingType + "\"," + "\"actionType\": \"" + actionType +"\"},";
		}

		vqeLarge.close();
		
		jsonString = jsonString.substring(0, jsonString.length() - 1);

		return jsonString;
	}
	
	private static String getContractSellers(VirtGraph graphEuFts, String contractUri) {
		
		String sellerPart = "";
		
    	boolean resultsFlag = false;

    	String querySellers = "PREFIX elod: <http://linkedeconomy.org/ontology#> " +
    			"PREFIX gr: <http://purl.org/goodrelations/v1#> " +
				"PREFIX pc: <http://purl.org/procurement/public-contracts#> " +
				"SELECT ?seller (SAMPLE(?name) AS ?name) (SAMPLE(?legalName) AS ?legalName) " +
				"FROM <" + QueryConfiguration.queryGraphEuFts + "> " +
				"FROM <" + QueryConfiguration.queryGraphOrganizations + "> " +
				"WHERE { " +
				"<" + contractUri +"> elod:seller ?seller . " +
				"?seller gr:name ?name . " +
				"OPTIONAL {" +
					"?seller gr:legalName ?legalName " +
				"}." +
				"}";
		
		VirtuosoQueryExecution vqeSellers = VirtuosoQueryExecutionFactory.create(querySellers, graphEuFts);
		ResultSet resultsSellers = vqeSellers.execSelect();
		
		while (resultsSellers.hasNext()) {
			QuerySolution result = resultsSellers.nextSolution();
			String sellerUri = result.getResource("seller").getURI();
			Literal sellerLegalName = result.getLiteral("legalName");
			String sellerName = result.getLiteral("name").getString();
			
			if (sellerLegalName != null) {
				sellerPart += "<a href=\\\"" + hm.urlProfilePage(sellerUri, "Seller") + 
			 			 	  "\\\" target=\\\"_blank\\\">" + hm.cleanInvalidCharsJsonData(sellerLegalName.getString()) + "</a>,";
			} else {
				sellerPart += "<a href=\\\"" + hm.urlProfilePage(sellerUri, "Seller") + 
		 			 	  	  "\\\" target=\\\"_blank\\\">" + hm.cleanInvalidCharsJsonData(sellerName) + "</a>,";
			}

			resultsFlag = true;
		}
		
		vqeSellers.close();
		
		if (!resultsFlag) {
			sellerPart += "N/A";
		} else {
			sellerPart = sellerPart.substring(0, sellerPart.length() - 1);
		}
		
		return sellerPart;
	}

}