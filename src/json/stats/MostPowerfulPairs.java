package json.stats;

import json.HelperMethods;
import json.QueriesCommon;
import json.QueryConfiguration;

import virtuoso.jena.driver.VirtGraph;
import virtuoso.jena.driver.VirtuosoQueryExecution;
import virtuoso.jena.driver.VirtuosoQueryExecutionFactory;

import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Literal;

/**
 * @author G. Razis
 */
public class MostPowerfulPairs {
	
	private static HelperMethods hm = new HelperMethods();
	
	public static void main(String[] args) {
		
		/* EuFts Graph */
		VirtGraph graphEuFts = new VirtGraph(QueryConfiguration.queryGraphEuFts, QueryConfiguration.connectionString, 
												  QueryConfiguration.username, QueryConfiguration.password);
		System.out.println("Connected to EuFts Graph!");
		
		/* Organizations Graph */
		VirtGraph graphOrgs = new VirtGraph(QueryConfiguration.queryGraphEuFts, QueryConfiguration.connectionString, 
											QueryConfiguration.username, QueryConfiguration.password);
		System.out.println("Connected to Organizations Graph!");
		
		/* initialize the JSON */
		String jsonString = "";

		/* start the 'couchDb' JSON */
		if (QueryConfiguration.couchDbUsage) {
			jsonString += "{\"docs\":";
		}

		/* start the 'normal' JSON */
		jsonString += "[{";

		/** create _id tag **/
		if (QueryConfiguration.couchDbUsage) {
			jsonString += "\"_id\": \"" + "eufts-mostPowerfulPairs" + "\",";
		}
		
		jsonString += "\"mostPowerfulPairs\": [";
		
		jsonString = getMostPowerfulPairs(graphEuFts, graphOrgs, jsonString);
		
		jsonString += "]";
		
		/* close the 'normal' JSON */
		jsonString += "}]";

		/* close the 'couchDb' JSON */
		if (QueryConfiguration.couchDbUsage) {
			jsonString += "}";
		}

		hm.writeJsonFile(jsonString, "others", "mostPowerfulPairs");	
		
		graphOrgs.close();
		graphEuFts.close();
		
		System.out.println("\nFinished!");
	}
	
	private static String getMostPowerfulPairs(VirtGraph graphEuFts, VirtGraph graphOrgs, String jsonString) {
		
		QueriesCommon qsCommon = new QueriesCommon();

		String queryPairs = "PREFIX elod: <http://linkedeconomy.org/ontology#> " +
				"PREFIX gr: <http://purl.org/goodrelations/v1#> " +
				"PREFIX pc: <http://purl.org/procurement/public-contracts#> " +
				"SELECT	?buyer ?buyerName ?seller (SUM(xsd:decimal(?amount)) AS ?totalAmount) " +
					   "(COUNT(DISTINCT ?payment) AS ?numberOfContracts) " +
				"FROM <" + QueryConfiguration.queryGraphEuFts + ">" +
				"WHERE { " +
				"?payment elod:hasExpenditureLine ?expLine ; " +
						 "elod:hasRelatedContract ?contract ; " +
						 "elod:buyer ?buyer . " +
				"?expLine elod:seller ?seller . " +
				"?contract pc:actualPrice ?actual . " +
				"?actual gr:hasCurrencyValue ?amount . " +
				"?buyer gr:legalName ?buyerName . " +
				"} " +
				"GROUP BY ?buyer ?buyerName ?seller " +
				"ORDER BY DESC(?totalAmount) " +
				"LIMIT 1000";

		VirtuosoQueryExecution vqePairs = VirtuosoQueryExecutionFactory.create(queryPairs, graphEuFts);
		ResultSet resultsPairs = vqePairs.execSelect();

		while (resultsPairs.hasNext()) {
			QuerySolution result = resultsPairs.nextSolution();
			String buyerUri = result.getResource("buyer").getURI();
			String buyerName = result.getLiteral("buyerName").getString();
			String sellerUri = result.getResource("seller").getURI();
			Literal contractAmountLit = result.getLiteral("totalAmount");
			String contractAmount = hm.convertAmount(contractAmountLit);
			String contracts = result.getLiteral("numberOfContracts").getString();
			
			String sellerName = "N/A";
			Literal sellerNameLit = qsCommon.getSellerLegalName(graphOrgs, sellerUri);
			
			if (sellerNameLit != null) {
				sellerName = sellerNameLit.getString();
			} else {
				sellerNameLit = qsCommon.getSellerName(graphOrgs, sellerUri);
				if (sellerNameLit != null) {
					sellerName = sellerNameLit.getString();
				} else {
					sellerNameLit = qsCommon.getSellerEuFtsName(graphEuFts, sellerUri);
					if (sellerNameLit != null) {
						sellerName = sellerNameLit.getString();
					}
				}
			}
			
			String buyerPart = "<a href=\\\"" + hm.urlProfilePage(buyerUri, "Buyer") + 
							   "\\\" target=\\\"_blank\\\">" + hm.cleanInvalidCharsJsonData(buyerName)+ "</a>";
			
			String sellerPart = "<a href=\\\"" + hm.urlProfilePage(sellerUri, "Seller") + 
						 		"\\\" target=\\\"_blank\\\">" + hm.cleanInvalidCharsJsonData(sellerName) + "</a>";
			
			jsonString += "{\"buyer\": \"" + buyerPart + "\"," + "\"seller\": \"" + sellerPart + "\"," + 
						  "\"contractAmount\": \"" + contractAmount + "\"," + "\"totalContracts\": \"" + 
						  contracts + "\"},";
		}

		vqePairs.close();
		
		jsonString = jsonString.substring(0, jsonString.length() - 1);

		return jsonString;
	}

}