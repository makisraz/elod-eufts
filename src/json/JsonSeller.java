package json;

import com.hp.hpl.jena.rdf.model.Resource;

import java.util.ArrayList;

import utils.Logger;

import virtuoso.jena.driver.VirtGraph;

/**
 * @author G. Razis
 */
public class JsonSeller {
	
	public static final String logFilename = "euFtsSellerJsonLog";
	
	public static void main(String[] args) {
		
		Logger.getInstance().write(logFilename, "Running **EuFts JSON Sellers**...");
		
		HelperMethods hm = new HelperMethods();
		QueriesSeller qsSeller = new QueriesSeller();
		QueriesCommon qsCommon = new QueriesCommon();
	
		/* EuFts Graph */
		VirtGraph graphEuFts = new VirtGraph(QueryConfiguration.queryGraphEuFts, QueryConfiguration.connectionString, 
											 QueryConfiguration.username, QueryConfiguration.password);
		System.out.println("Connected to EuFts Graph!");
		
		/* Organizations Graph */
		VirtGraph graphOrganizations = new VirtGraph(QueryConfiguration.queryGraphOrganizations, QueryConfiguration.connectionString, 
												     QueryConfiguration.username, QueryConfiguration.password);
		System.out.println("Connected to Organizations Graph!");
		
		/* Statistics Graph */
		VirtGraph graphStats = new VirtGraph(QueryConfiguration.queryGraphEuFtsStats, QueryConfiguration.connectionString, 
											 QueryConfiguration.username, QueryConfiguration.password);
		System.out.println("Connected to Eu Fts Statistics Graph!");
		
		/** Anadoxoi (Sellers) **/
		Logger.getInstance().write(logFilename, "Querying Anadoxous - Sellers");
		ArrayList<Resource> sellersList = new ArrayList<Resource>();
		sellersList = qsCommon.getOrgrsWithStats(graphStats, "Seller", QueryConfiguration.yearOfAgents, 
												 QueryConfiguration.referenceDuration, sellersList);
		
		String dateDataUpdated = hm.getFormattedDateFromDateLiteral(qsCommon.getDataLastDate(graphEuFts));
		
		Logger.getInstance().write(logFilename, "JSONs to be created: " + sellersList.size());

		for (Resource sellerUri : sellersList) {
				
			/* initialize the JSON */
			String jsonString = "";

			/* start the 'couchDb' JSON */
			if (QueryConfiguration.couchDbUsage) {
				jsonString += "{\"docs\":";
			}

			/* start the 'normal' JSON */
			jsonString += "[{";

			/** create _id tag **/
			if (QueryConfiguration.couchDbUsage) {
				jsonString += "\"_id\": \"" + hm.urlProfilePage(sellerUri.getURI(), "Seller") + "\",";
			}

			System.out.println("Querying Seller: " + sellerUri.getURI());
			jsonString = qsSeller.getSellerDetails(jsonString, graphOrganizations, sellerUri.getURI());

			jsonString += "\"spendingDetails\": [";

			long[] amCntr = {0, 0};

			/* 2007 to [previous (month's) year - 2] */
			for (int i = 2007; i < Integer.valueOf(QueryConfiguration.previousYear); i++) {
				amCntr = qsCommon.getOrgCategoryAmount(graphStats, sellerUri.getURI(), "Payment", "Seller", 
													   String.valueOf(i), QueryConfiguration.referenceDuration, amCntr);
			}

			jsonString += "{\"referencePeriod\": \"2007\"," + "\"amount\": \"€" + hm.convertIntegerAmount(amCntr[0]) + 
						  "\"," + "\"counter\": \"" + amCntr[1] + "\"},";

			/* [previous year] */
			jsonString = qsCommon.getOrgCategoryStats(jsonString, graphStats, sellerUri.getURI(), "Payment", "Seller", 
													  QueryConfiguration.previousYear, QueryConfiguration.referenceDuration, true);
			/* [current year] */
			jsonString = qsCommon.getOrgCategoryStats(jsonString, graphStats, sellerUri.getURI(), "Payment", "Seller",
													  QueryConfiguration.currentYear, QueryConfiguration.referenceDuration, true);

			jsonString = jsonString.substring(0, jsonString.length() - 1) + "],";

			System.out.println("Querying Spending Items of: " + sellerUri.getURI());
			jsonString = qsSeller.getSellerSpendingItems(jsonString, graphEuFts, graphOrganizations, sellerUri.getURI());

			/** About **/
			jsonString = qsCommon.addAbout(jsonString, dateDataUpdated);

			/* close the 'normal' JSON */
			jsonString += "}]";

			/* close the 'couchDb' JSON */
			if (QueryConfiguration.couchDbUsage) {
				jsonString += "}";
			}

			hm.writeJsonFile(jsonString, "sellers", sellerUri.getURI());				
		}
		
		graphStats.close();
		graphEuFts.close();
		graphOrganizations.close();
		
		Logger.getInstance().write(logFilename, "Finished!\n");
	}
}