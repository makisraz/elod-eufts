package json;

import virtuoso.jena.driver.VirtGraph;
import virtuoso.jena.driver.VirtuosoQueryExecution;
import virtuoso.jena.driver.VirtuosoQueryExecutionFactory;

import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Literal;

/**
 * @author G. Razis
 */
public class QueriesBuyer {
	
	private HelperMethods hm = new HelperMethods();
	
    public String getBuyerDetails(String jsonString, VirtGraph graphOrganizations, String buyerUri) {
		
    	boolean resultsFlag = false;

    	String queryOrg = "PREFIX elod: <http://linkedeconomy.org/ontology#> " +
				"PREFIX gr: <http://purl.org/goodrelations/v1#> " +
				"PREFIX vcard: <http://www.w3.org/2006/vcard/ns#> " +
				"SELECT DISTINCT ?legalName " +
				"FROM <" + QueryConfiguration.queryGraphEuFts + "> " +
				"WHERE { " +
				"<" + buyerUri +"> gr:legalName ?legalName . " +
				"}";
		
		VirtuosoQueryExecution vqeOrg = VirtuosoQueryExecutionFactory.create(queryOrg, graphOrganizations);
		ResultSet resultsOrg = vqeOrg.execSelect();
		
		jsonString += "\"buyerDtls\": ";
		
		if (resultsOrg.hasNext()) {
			QuerySolution result = resultsOrg.nextSolution();
			Literal buyerName = result.getLiteral("legalName");
			
			if(buyerName != null) {
				jsonString += "{\"buyer\": \"" + buyerName.getString().trim()  + "\"},";
			}
			else {
				jsonString += "{\"buyer\": \"" + "-"  + "\"},";
			}

			vqeOrg.close();
			resultsFlag = true;
		}
		
		if (!resultsFlag) {
			jsonString += "{},";
		}
		
		return jsonString;
	}
    
	public String getBuyerSpendingItems(String jsonString, VirtGraph graphEuFts, VirtGraph graphOrganizations, String buyerUri) {
		
		boolean resultsFlag = false;
		
		String queryPaymentsItems = "PREFIX elod: <http://linkedeconomy.org/ontology#> " +
									"PREFIX dcterms: <http://purl.org/dc/terms/> " +
									"PREFIX gr: <http://purl.org/goodrelations/v1#> " +
									"SELECT DISTINCT ?seller ?date ?contractId ?sellerName (SAMPLE (?buyerName) AS ?buyerName) xsd:decimal(?amount) AS ?paymentAmount xsd:decimal(?totalAmount) AS ?totalAmount ?description " + 
									"FROM <" + QueryConfiguration.queryGraphEuFts + "> " +
									"FROM <" + QueryConfiguration.queryGraphOrganizations + "> " +
									"WHERE { " +
									"?spendingItem elod:hasExpenditureLine ?expLine ; " +
												  "elod:hasRelatedContract ?contract . " +
									"?contract elod:buyer <" + buyerUri + "> ; " +
											  "elod:contractId ?contractId ; " +
											  "pc:item ?offering ; " +
											  "dcterms:issued ?date ; " +
											  "pc:actualPrice ?currentAmount . " +
									"<" + buyerUri + "> gr:legalName ?buyerName . " +
									"?expLine elod:amount ?ups ; " +
											 "elod:seller ?seller . " +
									"?ups gr:hasCurrencyValue ?amount . " +
									"?seller gr:name ?sellerName . " +
								    "?offering gr:includesObject ?typeAndQuantityNode . " +
									"?typeAndQuantityNode gr:typeOfGood ?typeOfGood . " +
									"?typeOfGood gr:description ?description. " +
									"?currentAmount gr:hasCurrencyValue ?totalAmount . " +
									"FILTER (?date >= \"2007\"^^xsd:gYear) . " +
									"FILTER (?date <= \"" + String.valueOf(Integer.valueOf(QueryConfiguration.currentYear)) + "\"^^xsd:gYear) . " +
									"}";

		VirtuosoQueryExecution vqePaymentsItems = VirtuosoQueryExecutionFactory.create(queryPaymentsItems, graphEuFts);
		ResultSet resultsPaymentsItems = vqePaymentsItems.execSelect();		
		
		jsonString += "\"sellerSpendingItems\": [";
		
		while (resultsPaymentsItems.hasNext()) {
			QuerySolution result = resultsPaymentsItems.nextSolution();
			String date = result.getLiteral("date").getString().split("-")[0];
			String contractId = result.getLiteral("contractId").getString();
			String paymentAmount = hm.roundAmountNoDecimals(result.getLiteral("paymentAmount"));
			String totalAmount = hm.roundAmountNoDecimals(result.getLiteral("totalAmount"));
			Literal descriptionLiteral = result.getLiteral("description");
			String description;
			if (descriptionLiteral != null) {
				description = descriptionLiteral.getString().replace("\"", "");
			}
			else {
				description = "-";
			}
			
			String buyerLegalName = result.getLiteral("buyerName").getString();
			String buyerPart = "<a href=\\\"" + hm.urlProfilePage(buyerUri, "Buyer") + 
							   "\\\" target=\\\"_blank\\\">" + hm.cleanInvalidCharsJsonData(buyerLegalName) + "</a>";
			
			String sellerName = result.getLiteral("sellerName").getString();
			String sellerPart = "<a href=\\\"" + hm.urlProfilePage(result.getResource("seller").toString(), "Seller") + 
						 		"\\\" target=\\\"_blank\\\">" + hm.cleanInvalidCharsJsonData(sellerName) + "</a>";
			
			jsonString += "{\"date\": \"" + date + "\"," + "\"contractId\": \"" + contractId + "\"," + 
						  "\"buyerLegalName\": \"" + buyerPart + "\"," + "\"sellerName\": \"" + sellerPart + 
						  "\"," + "\"paymentAmount\": \"" + paymentAmount + "\"," + "\"totalAmount\": \"" + 
						  totalAmount + "\"," + "\"description\": \"" + description;

			jsonString += "\"},";
			
			resultsFlag = true;
		}
		
		vqePaymentsItems.close();
		
		if (resultsFlag) {
			jsonString = jsonString.substring(0, jsonString.length() - 1) + "],";
		} else {
			jsonString = jsonString.substring(0, jsonString.length()) + "],";
		}
		
		return jsonString;
	}
}