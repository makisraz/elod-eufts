package json;

import java.util.ArrayList;

import virtuoso.jena.driver.VirtGraph;
import virtuoso.jena.driver.VirtuosoQueryExecution;
import virtuoso.jena.driver.VirtuosoQueryExecutionFactory;

import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.Resource;

/**
 * @author G. Razis
 */
public class QueriesCommon {
	
	private HelperMethods hm = new HelperMethods();
	
	/** queries regarding the Statistics **/
	public ArrayList<Resource> getOrgrsWithStats(VirtGraph graphStats, String buyerOrSeller, String referenceTime, 
												 String duration, ArrayList<Resource> orgsList) {

		String queryOrgs = "PREFIX elod: <http://linkedeconomy.org/ontology#> " +
					"SELECT DISTINCT ?org " +
					"FROM <" + QueryConfiguration.queryGraphEuFtsStats + "> " +
					"WHERE { " +
					"?statistic elod:isStatisticOf ?org ; " +
							   "elod:referenceTime ?referenceTime ; " +
							   "elod:referenceTimeDuration ?duration ; " +
							   "elod:orgActivity ?activity . " +
					"FILTER (?duration = \"" + duration + "\"^^xsd:duration) . " +
					"FILTER (?referenceTime = \"" + referenceTime + "\"^^xsd:gYear) . " +
					"FILTER (?activity = \"" + buyerOrSeller + "\"^^xsd:string) . " +
					//"FILTER (?org = <http://linkedeconomy.org/resource/Organization/094019245>) . " +
					"} limit 10";

		VirtuosoQueryExecution vqeOrgs = VirtuosoQueryExecutionFactory.create(queryOrgs, graphStats);
		ResultSet resultsOrgs = vqeOrgs.execSelect();
		
		while (resultsOrgs.hasNext()) {
			QuerySolution result = resultsOrgs.nextSolution();
			Resource buyerUri = result.getResource("org");
			if (!orgsList.contains(buyerUri) && !buyerUri.getURI().contains(" ") && 
				!buyerUri.getURI().equalsIgnoreCase("http://linkedeconomy.org/resource/Organization/")) {
				orgsList.add(buyerUri);
			}
		}
		
		vqeOrgs.close();
		
		return orgsList;
	}
	
	public String getOrgCategoryStats(String jsonString, VirtGraph graphStats, String buyerOrSellerUri, String category, 
									  String buyerOrSeller, String referenceTime, String duration, boolean yearFlag) {
		
		String amountCategory = null;
		Literal amountCategoryVariation = null;
		Literal counterCategory = null;
		String counterCategoryStr = null;
		Literal counterCategoryVariation = null;
		String rankCategory = null;
		Literal rankCategoryVariation = null;
		
		String amountCategoryVariationStr = null;
		String counterCategoryVariationStr = null;
		String rankCategoryVariationStr = null;
		
		String queryStats = "PREFIX elod: <http://linkedeconomy.org/ontology#> " +
							"SELECT ?statistic ?aggregateRes (xsd:decimal(?amount) as ?amount) ?amountVar ?counterRes ?counter " +
							"?counterVar ?rankRes ?rank ?rankVar ?category ?activity " +
							"FROM <" + QueryConfiguration.queryGraphEuFtsStats + "> " +
							"WHERE { " +
							"?statistic elod:isStatisticOf <" + buyerOrSellerUri + "> ; " +
									   "elod:referenceTime ?referenceTime ; " +
									   "elod:referenceTimeDuration ?duration ; " +
									   "elod:hasAggregate ?aggregateRes ; " +
									   "elod:hasCounter ?counterRes ; " +
									   "elod:hasRank ?rankRes ; " +
									   "elod:orgActivity ?activity . " +
							"?aggregateRes elod:hasCategory ?category ; " +
										  "elod:aggregatedAmount ?amount . " +
							"?counterRes elod:hasCategory ?category ; " +
										"elod:counter ?counter . " +
							"?rankRes elod:hasCategory ?category ; " +
									 "elod:rank ?rank . " +
							"OPTIONAL { " +
								"?aggregateRes elod:variation ?amountVar . " +
							"} . " +
							"OPTIONAL { " +
								"?counterRes elod:variation ?counterVar . " +
							"} . " +
							"OPTIONAL { " +
								"?rankRes elod:variation ?rankVar . " +
							"} . " +
							"FILTER (?referenceTime = \"" + referenceTime + "\"^^xsd:gYear) . " +
							"FILTER (?duration = \"" + duration + "\"^^xsd:duration) . " +
							"FILTER (?category = <http://linkedeconomy.org/ontology#" + category + ">) . " +
							"FILTER (?activity = \"" + buyerOrSeller + "\"^^xsd:string) . " +
							"}";

		VirtuosoQueryExecution vqeStats = VirtuosoQueryExecutionFactory.create(queryStats, graphStats);	
		ResultSet resultsStats = vqeStats.execSelect();	
		
		if (resultsStats.hasNext()) {
			QuerySolution result = resultsStats.nextSolution();
			Literal amountCategoryLit = result.getLiteral("amount");
			amountCategoryVariation = result.getLiteral("amountVar");
			counterCategory = result.getLiteral("counter");
			counterCategoryVariation = result.getLiteral("counterVar");
			rankCategory = result.getLiteral("rank").getString();
			rankCategoryVariation = result.getLiteral("rankVar");
			amountCategory = hm.convertAmount(amountCategoryLit);
		}
		
		vqeStats.close();
		
		if (rankCategoryVariation != null) {
			if ( (rankCategoryVariation.getString().equalsIgnoreCase("-999999999")) || 
				 (Integer.valueOf(rankCategoryVariation.getString()) == Integer.MIN_VALUE) ) {
				rankCategoryVariationStr = "-";
			} else {
				rankCategoryVariationStr = rankCategoryVariation.getString();
			}
		} else {
			rankCategoryVariationStr = "-";
		}
		
		if (rankCategory != null) {
			if (rankCategory.equalsIgnoreCase("-1")) {
				rankCategory = "-";
			}
		} else {
			rankCategory = "-";
		}
		
		if (counterCategory != null) {
			counterCategoryStr = counterCategory.getString();
		} else {
			counterCategoryStr = "0";
		}
		
		if (amountCategory == null) {
			amountCategory = "0.0K";
		}
		
		if (counterCategoryVariation != null) {
			if (counterCategoryVariation.getString().equalsIgnoreCase("-1")) {
				counterCategoryVariationStr = "-";
			} else {
				counterCategoryVariationStr = counterCategoryVariation.getString();
			}
		} else {
			counterCategoryVariationStr = "-";
		}
		
		if (amountCategoryVariation != null) {
			if (Float.parseFloat(amountCategoryVariation.getString()) == Float.POSITIVE_INFINITY) {
				amountCategoryVariationStr = "-";
			} else {
				amountCategoryVariationStr = amountCategoryVariation.getString();
			}
		} else {
			amountCategoryVariationStr = "-";
		}
		
		if (referenceTime.contains("+")) { //Used only in OLV 7
			referenceTime = referenceTime.split("\\+")[0];
		}
		
		jsonString += "{\"referencePeriod\": \"" + referenceTime + "\"," + "\"amount\": \"€" + amountCategory + "\"," + 
					  "\"amountVariation\": \"" + amountCategoryVariationStr + "\"," + "\"counter\": \"" + 
					  counterCategoryStr + "\"," + "\"counterVariation\": \"" + counterCategoryVariationStr + "\"," + 
					  "\"rank\": \"" + rankCategory + "\"," + "\"rankVariation\": \"" + rankCategoryVariationStr + "\"},";
		
		return jsonString;
	}
	
	public long[] getOrgCategoryAmount(VirtGraph graphStats, String buyerOrSellerUri, String category, String buyerOrSeller, 
									   String referenceTime, String duration, long[] amCntr) {
		
		String queryStats = "PREFIX elod: <http://linkedeconomy.org/ontology#> " +
				"SELECT ?statistic ?aggregateRes (xsd:decimal(?amount) as ?amount) ?counterRes ?counter ?category ?activity " +
				"FROM <" + QueryConfiguration.queryGraphEuFtsStats + "> " +
				"WHERE { " +
				"?statistic elod:isStatisticOf <" + buyerOrSellerUri + "> ; " +
						   "elod:referenceTime ?referenceTime ; " +
						   "elod:referenceTimeDuration ?duration ; " +
						   "elod:hasAggregate ?aggregateRes ; " +
						   "elod:hasCounter ?counterRes ; " +
						   "elod:orgActivity ?activity . " +
				"?aggregateRes elod:hasCategory ?category ; " +
							  "elod:aggregatedAmount ?amount . " +
				"?counterRes elod:hasCategory ?category ; " +
							"elod:counter ?counter . " +
				"FILTER (?referenceTime = \"" + referenceTime + "\"^^xsd:gYear) . " +
				"FILTER (?duration = \"" + duration + "\"^^xsd:duration) . " +
				"FILTER (?category = <http://linkedeconomy.org/ontology#" + category + ">) . " +
				"FILTER (?activity = \"" + buyerOrSeller + "\"^^xsd:string) . " +
				"}";

		VirtuosoQueryExecution vqeStats = VirtuosoQueryExecutionFactory.create(queryStats, graphStats);
		ResultSet resultsStats = vqeStats.execSelect();

		if (resultsStats.hasNext()) {
			QuerySolution result = resultsStats.nextSolution();
			amCntr[0] += result.getLiteral("amount").getLong();
			amCntr[1] += result.getLiteral("counter").getLong();
		}

		vqeStats.close();

		return amCntr;
	}
	
	/** queries regarding the names **/
	public Literal getSellerLegalName(VirtGraph graphOrganizations, String sellerUri) {
		
		Literal orgLegalName = null;
		
		if (checkUri(sellerUri)) {
			String queryOrg = "PREFIX gr: <http://purl.org/goodrelations/v1#> " +
			  		  "SELECT ?legalName " +
			  		  "FROM <" + QueryConfiguration.queryGraphOrganizations + "> " +
			  		  "WHERE { " +
			  		  "<" + sellerUri +"> gr:legalName ?legalName . " +
			  		  "}";
	
			VirtuosoQueryExecution vqeOrgName = VirtuosoQueryExecutionFactory.create(queryOrg, graphOrganizations);		
			ResultSet resultsOrgName = vqeOrgName.execSelect();
			
			if (resultsOrgName.hasNext()) {
				QuerySolution result = resultsOrgName.nextSolution();
				orgLegalName = result.getLiteral("legalName");
			}
			
			vqeOrgName.close();
		}
		
		return orgLegalName;
	}
	
	public Literal getSellerName(VirtGraph graphOrganizations, String sellerUri) {
		
		Literal orgName = null;
		
		if (checkUri(sellerUri)) {
			String queryOrg = "PREFIX gr: <http://purl.org/goodrelations/v1#> " +
			  		  "SELECT ?name " +
			  		  "FROM <" + QueryConfiguration.queryGraphOrganizations + "> " +
			  		  "WHERE { " +
			  		  "<" + sellerUri +"> gr:name ?name . " +
			  		  "}";
	
			VirtuosoQueryExecution vqeOrgName = VirtuosoQueryExecutionFactory.create(queryOrg, graphOrganizations);		
			ResultSet resultsOrgName = vqeOrgName.execSelect();
			
			if (resultsOrgName.hasNext()) {
				QuerySolution result = resultsOrgName.nextSolution();
				orgName = result.getLiteral("name");
			}
			
			vqeOrgName.close();
		}
		
		return orgName;
	}
	
	public Literal getSellerEuFtsName(VirtGraph graphEuFts, String sellerUri) {
		
		Literal orgName = null;
		
		if (checkUri(sellerUri)) {
			String queryOrg = "PREFIX gr: <http://purl.org/goodrelations/v1#> " +
			  		  "SELECT ?name " +
			  		  "FROM <" + QueryConfiguration.queryGraphEuFts + "> " +
			  		  "WHERE { " +
			  		  "<" + sellerUri +"> gr:name ?name . " +
			  		  "}";
	
			VirtuosoQueryExecution vqeOrgName = VirtuosoQueryExecutionFactory.create(queryOrg, graphEuFts);		
			ResultSet resultsOrgName = vqeOrgName.execSelect();
			
			if (resultsOrgName.hasNext()) {
				QuerySolution result = resultsOrgName.nextSolution();
				orgName = result.getLiteral("name");
			}
			
			vqeOrgName.close();
		}
		
		return orgName;
	}
	
	public Literal getBuyerLegalName(VirtGraph graphOrganizations, String buyerUri) {
		
		Literal orgName = null;
		
		if (checkUri(buyerUri)) {
			String queryName = "PREFIX gr: <http://purl.org/goodrelations/v1#> " +
			  		  "SELECT ?legalName " +
			  		  "FROM <" + QueryConfiguration.queryGraphOrganizations + "> " +
			  		  "WHERE { " +
			  		  "<" + buyerUri +"> gr:legalName ?legalName . " +
			  		  "}";
	
			VirtuosoQueryExecution vqeName = VirtuosoQueryExecutionFactory.create(queryName, graphOrganizations);		
			ResultSet resultsName = vqeName.execSelect();
			
			while (resultsName.hasNext()) {
				QuerySolution result = resultsName.nextSolution();
				orgName = result.getLiteral("legalName");
			}
			
			vqeName.close();
		}
		
		return orgName;
	}
	
	public Literal getBuyerName(VirtGraph graphOrganizations, String buyerUri) {
		
		Literal orgName = null;
		
		if (checkUri(buyerUri)) {
			String queryName = "PREFIX gr: <http://purl.org/goodrelations/v1#> " +
			  		  "SELECT ?name " +
			  		  "FROM <" + QueryConfiguration.queryGraphOrganizations + "> " +
			  		  "WHERE { " +
			  		  "<" + buyerUri +"> gr:name ?name . " +
			  		  "}";
	
			VirtuosoQueryExecution vqeName = VirtuosoQueryExecutionFactory.create(queryName, graphOrganizations);		
			ResultSet resultsName = vqeName.execSelect();
			
			while (resultsName.hasNext()) {
				QuerySolution result = resultsName.nextSolution();
				orgName = result.getLiteral("name");
			}
			
			vqeName.close();
		}
		
		return orgName;
	}
	
	public Literal getOrganizationUnitName(VirtGraph graphOrganizations, String buyerUnitUri) {
		
		Literal orgName = null;
		
		if (checkUri(buyerUnitUri)) {
			String queryName = "SELECT ?name " +
				  		  "FROM <" + QueryConfiguration.queryGraphOrganizations + "> " +
				  		  "WHERE { " +
				  		  "<" + buyerUnitUri +"> rdfs:label ?name . " +
				  		  "}";
	
			VirtuosoQueryExecution vqeName = VirtuosoQueryExecutionFactory.create(queryName, graphOrganizations);		
			ResultSet resultsName = vqeName.execSelect();
			
			while (resultsName.hasNext()) {
				QuerySolution result = resultsName.nextSolution();
				orgName = result.getLiteral("name");
			}
			
			vqeName.close();
		}
		
		return orgName;
	}
	
	public String getOrganizationTranslatedName(VirtGraph graphOrganizations, String orgUri) {
		
		String orgTransName = null;
		
		if (checkUri(orgUri)) {
			String queryOrg = "PREFIX elod: <http://linkedeconomy.org/ontology#> " +
			  		  "SELECT ?translation " +
			  		  "FROM <" + QueryConfiguration.queryGraphOrganizations + "> " +
			  		  "WHERE { " +
			  		  "<" + orgUri +"> elod:translation ?translation . " +
			  		  "}";
	
			VirtuosoQueryExecution vqeOrgName = VirtuosoQueryExecutionFactory.create(queryOrg, graphOrganizations);		
			ResultSet resultsOrgName = vqeOrgName.execSelect();
			
			if (resultsOrgName.hasNext()) {
				QuerySolution result = resultsOrgName.nextSolution();
				orgTransName = result.getLiteral("translation").getString();
			}
			
			vqeOrgName.close();
		}
		
		return orgTransName;
	}
	
	public Literal getPersonName(VirtGraph graphOrganizations, String personUri) {
		
		Literal personName = null;
		
		if (checkUri(personUri)) {
			String queryName = "PREFIX gr: <http://purl.org/goodrelations/v1#> " +
						  "SELECT ?name " +
				  		  "FROM <" + QueryConfiguration.queryGraphOrganizations + "> " +
				  		  "WHERE { " +
				  		  "<" + personUri +"> gr:name ?name . " +
				  		  "}";
	
			VirtuosoQueryExecution vqeName = VirtuosoQueryExecutionFactory.create(queryName, graphOrganizations);		
			ResultSet resultsName = vqeName.execSelect();
			
			while (resultsName.hasNext()) {
				QuerySolution result = resultsName.nextSolution();
				personName = result.getLiteral("name");
			}
			
			vqeName.close();
		}
		
		return personName;
	}
	
	public boolean checkUri(String uri) {
		
		boolean correctUriFlag = true;
		
		if (uri.equalsIgnoreCase("http://linkedeconomy.org/resource/Organization/")) {
			correctUriFlag = false;
		} else if (uri.equalsIgnoreCase("http://linkedeconomy.org/resource/OrganizationalUnit/")) {
			correctUriFlag = false;
		} else if (uri.equalsIgnoreCase("http://linkedeconomy.org/resource/Person/")) {
			correctUriFlag = false;
		} else if ( uri.equalsIgnoreCase("http://linkedeconomy.org/resource/Organization/..") || 
				    uri.equalsIgnoreCase("http://linkedeconomy.org/resource/Organization/.") ) {
			correctUriFlag = false;
		}
		
		if ( uri.contains(" ") || uri.contains("`") || uri.contains("\"") ) {
			correctUriFlag = false;
		}
		
		return correctUriFlag;
	}
	
	public String configureBuyerOrgOrUnitName(VirtGraph graphOrganizations, Literal buyerlegalName, Resource orgUri) {
		
		String buyerNameStr = "";
		
		if (buyerlegalName != null) { //Legal Name
			buyerNameStr = hm.cleanInvalidCharsJsonData(buyerlegalName.getString());
		} else { //Name
			Literal buyerName = getBuyerName(graphOrganizations, orgUri.getURI());
			if (buyerName != null) {
				buyerNameStr = hm.cleanInvalidCharsJsonData(buyerName.getString());
			} else {
				Literal buyerUnitName = getOrganizationUnitName(graphOrganizations, orgUri.getURI());
				if (buyerUnitName != null) { //Label
					buyerNameStr = hm.cleanInvalidCharsJsonData(buyerUnitName.getString());
					hm.writeToFile("label", orgUri.getURI());
				} else { //Nothing
					//try using the VatID
					buyerNameStr = getAnyNameByVatId(graphOrganizations, orgUri.getURI());
					if (buyerNameStr == null) {
						buyerNameStr = "Name not found";
						hm.writeToFile("nameNotFound", orgUri.getURI());
					} else {
						buyerNameStr = hm.cleanInvalidCharsJsonData(buyerNameStr);
					}
				}
			}
		}
		
		return buyerNameStr.trim();
	}
	
	public String configureSellerOrgOrUnitOrPersonName(VirtGraph graphOrganizations, Literal sellerName, Resource sellerUri) {
		
		String sellerNameStr = "";
		
		if (sellerName != null) {
			sellerNameStr = hm.cleanInvalidCharsJsonData(sellerName.getString());
		} else {
			Literal sellerUnitName = getOrganizationUnitName(graphOrganizations, sellerUri.getURI());
			if (sellerUnitName != null) {
				sellerNameStr = hm.cleanInvalidCharsJsonData(sellerUnitName.getString());
			} else {
				Literal sellerPersonName = getPersonName(graphOrganizations, sellerUri.getURI());
				if (sellerPersonName != null) {
					sellerNameStr = hm.cleanInvalidCharsJsonData(sellerPersonName.getString());
				} else {
					sellerNameStr = getAnyNameByVatId(graphOrganizations, sellerUri.getURI());
					if (sellerNameStr == null) {
						sellerNameStr = "Name not found";
						hm.writeToFile("nameNotFound", sellerUri.getURI());
					} else {
						sellerNameStr = hm.cleanInvalidCharsJsonData(sellerNameStr);
					}
				}
			}
		}
		
		return sellerNameStr.trim();
	}
	
	public String getAnyNameByVatId(VirtGraph graphOrganizations, String buyerUri) {
		//case: http://linkedeconomy.org/resource/Organization/099260382 - http://linkedeconomy.org/resource/Organization/099260382/99219998
		
		String finalName = null;
		String legalNameStr = null;
		String nameStr = null;
		String labelNameStr = null;
		
		if (checkUri(buyerUri)) {
			
			String vatId = hm.getVatIdFromUri(buyerUri);
			
			String queryName = "PREFIX gr: <http://purl.org/goodrelations/v1#> " +
			  		  "SELECT ?legalName ?name ?labelName " +
			  		  "FROM <" + QueryConfiguration.queryGraphOrganizations + "> " +
			  		  "WHERE { " +
			  		  	"?orgUri gr:vatID \"" + vatId + "\"^^<http://www.w3.org/2001/XMLSchema#string> . " +
			  		  "OPTIONAL { " +
			  		  	"?orgUri gr:legalName ?legalName " +
			  		  "}. " +
			  		  "OPTIONAL { " +
			  		  	"?orgUri gr:name ?name " +
			  		  "}. " +
			  		  "OPTIONAL { " +
			  		  	"?orgUri rdfs:label ?labelName " +
			  		  "}. " +
			  		  "}";
	
			VirtuosoQueryExecution vqeName = VirtuosoQueryExecutionFactory.create(queryName, graphOrganizations);		
			ResultSet resultsName = vqeName.execSelect();
			
			while (resultsName.hasNext()) {
				QuerySolution result = resultsName.nextSolution();
				Literal legalName = result.getLiteral("legalName");
				Literal name = result.getLiteral("name");
				Literal labelName = result.getLiteral("labelName");
				
				if (legalName != null) {
					legalNameStr = legalName.getString();
				} else if (name != null) {
					nameStr = name.getString();
				} else if (labelName != null) {
					labelNameStr = labelName.getString();
				}
			}
			
			vqeName.close();
			
			if (legalNameStr != null) {
				finalName = legalNameStr;
			} else if (nameStr != null) {
				finalName = nameStr;
			} else if (labelNameStr != null) {
				finalName = labelNameStr;
			}
			
		}
		
		return finalName;
	}
	
	/** About **/
	public String addAbout(String jsonString, String dataDateUpdated) {
		
		jsonString += "\"about\": ";
		
		jsonString += "{\"lastUpdate\": \"" + dataDateUpdated + "\",";
		
		jsonString += "\"source\": \"<a href=\\\"http://ec.europa.eu/budget/fts/index_en.htm\\\">Σύστημα Δημοσιονομικής Διαφάνειας</a>" + "\"," + 
					  "\"sourceEng\": \"<a href=\\\"a href='http://ec.europa.eu/budget/fts/index_en.htm\\\">Financial Transparency System</a>";
		
		jsonString += "\"," + "\"termOfUse\": \"<a href=\\\"http://ec.europa.eu/geninfo/legal_notices_el.htm#copyright\\\">" + 
				   	  "Πνευματική Ιδιοκτησία</a>" + "\"," + 
					  "\"termOfUseEng\": \"<a href=\\\"http://ec.europa.eu/geninfo/legal_notices_en.htm#copyright\\\">" + 
					  "Copyright notice</a>";

		jsonString += "\"," + "\"problemsManagement\": \"" + "TBA\"}";
		
		return jsonString;
	}
	
	public Literal getDataLastDate(VirtGraph graphDiavgeiaII) {
		
		Literal date = null;
		
		String queryDate = "PREFIX dcterms: <http://purl.org/dc/terms/> " +
					"SELECT ?issueDate " +
					"FROM <" + QueryConfiguration.queryGraphEuFts + "> " +
					"WHERE { " +
					"?contract dcterms:issued ?issueDate . " +
					"} " +
					"ORDER BY DESC (?issueDate) " +
					"LIMIT 1";

		VirtuosoQueryExecution vqeDate = VirtuosoQueryExecutionFactory.create(queryDate, graphDiavgeiaII);	
		ResultSet resultsDate = vqeDate.execSelect();	
		
		if (resultsDate.hasNext()) {
			QuerySolution result = resultsDate.nextSolution();
			date = result.getLiteral("issueDate");
		}
		
		vqeDate.close();
		
		return date;
	}
	
}