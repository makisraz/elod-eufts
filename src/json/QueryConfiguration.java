package json;

/**
 * @author G. Razis
 */
public class QueryConfiguration {

	public static final String queryGraphEuFts = "http://linkedeconomy.org/EuFts";
	public static final String queryGraphOrganizations = "http://linkedeconomy.org/Organizations";
	public static final String queryGraphEuFtsStats = "http://linkedeconomy.org/EuFtsStatistics";
	
	public static final String connectionString = "jdbc:virtuoso://143.233.226.49:1111/autoReconnect=true/charset=UTF-8/log_enable=2";
	
	public static final String username = "dba";
	public static final String password = "d3ll0lv@69";
	
	public static final String yearOfAgents = "2014";
	public static final String currentYear = "2015";//String.valueOf(HelperMethods.getCurrentYear());
	public static final String referenceDuration = "P1Y";
	public static final String previousYear = "2014";//HelperMethods.getAskedYearWithInput(currentYear, 1);
	
	public static final String jsonFilepath = "C:/Users/Makis/Desktop/eprocur/JSON/"; //"/home/makis/JSON/eufts/couchDb/final/";
	
	public static final boolean couchDbUsage = true;
}