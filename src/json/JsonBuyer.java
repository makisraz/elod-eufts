package json;

import com.hp.hpl.jena.rdf.model.Resource;

import java.util.ArrayList;

import utils.Logger;

import virtuoso.jena.driver.VirtGraph;

/**
 * @author G. Razis
 */
public class JsonBuyer {
	
	public static final String logFilename = "euFtsBuyerJsonLog";
	
	public static void main(String[] args) {

		Logger.getInstance().write(logFilename, "Running **EuFts JSON Buyers**...");
		
		HelperMethods hm = new HelperMethods();
		QueriesBuyer qsBuyer = new QueriesBuyer();
		QueriesCommon qsCommon = new QueriesCommon();
	
		/* EuFts Graph */
		VirtGraph graphEuFts = new VirtGraph(QueryConfiguration.queryGraphEuFts, QueryConfiguration.connectionString, 
												  QueryConfiguration.username, QueryConfiguration.password);
		System.out.println("Connected to EuFts Graph!");
		
		/* Organizations Graph */
		VirtGraph graphOrganizations = new VirtGraph(QueryConfiguration.queryGraphOrganizations, QueryConfiguration.connectionString, 
												  QueryConfiguration.username, QueryConfiguration.password);
		System.out.println("Connected to Organizations Graph!");
		
		/* Statistics Graph */
		VirtGraph graphStats = new VirtGraph(QueryConfiguration.queryGraphEuFtsStats, QueryConfiguration.connectionString, 
											 QueryConfiguration.username, QueryConfiguration.password);
		System.out.println("Connected to Eu Fts Statistics Graph!");
		
		/** Foreis (Buyers) **/
		Logger.getInstance().write(logFilename, "Querying Foreis - Buyers");
		ArrayList<Resource> buyersList = new ArrayList<Resource>();
		buyersList = qsCommon.getOrgrsWithStats(graphStats, "Buyer", QueryConfiguration.yearOfAgents, 
												 QueryConfiguration.referenceDuration, buyersList);
		
		String dateDataUpdated = hm.getFormattedDateFromDateLiteral(qsCommon.getDataLastDate(graphEuFts));
		
		Logger.getInstance().write(logFilename, "JSONs to be created: " + buyersList.size());

		for (Resource buyerUri : buyersList) {
				
			/* initialize the JSON */
			String jsonString = "";

			/* start the 'couchDb' JSON */
			if (QueryConfiguration.couchDbUsage) {
				jsonString += "{\"docs\":";
			}

			/* start the 'normal' JSON */
			jsonString += "[{";

			/** create _id tag **/
			if (QueryConfiguration.couchDbUsage) {
				jsonString += "\"_id\": \"" + hm.urlProfilePage(buyerUri.getURI(), "Buyer") + "\",";
			}

			System.out.println("Querying Buyer: " + buyerUri.getURI());
			jsonString = qsBuyer.getBuyerDetails(jsonString, graphOrganizations, buyerUri.getURI());

			jsonString += "\"spendingDetails\": [";
			
			long[] amCntr = {0, 0};
			
			/* 2007 to [previous (month's) year - 2] */
			for (int i = 2007; i < Integer.valueOf(QueryConfiguration.previousYear); i++) {
				amCntr = qsCommon.getOrgCategoryAmount(graphStats, buyerUri.getURI(), "Payment", "Buyer", 
													   String.valueOf(i), QueryConfiguration.referenceDuration, amCntr);
			}

			jsonString += "{\"referencePeriod\": \"2007\"," + "\"amount\": \"€" + hm.convertIntegerAmount(amCntr[0]) + 
						  "\"," + "\"counter\": \"" + amCntr[1] + "\"},";

			/* [previous year] */
			jsonString = qsCommon.getOrgCategoryStats(jsonString, graphStats, buyerUri.getURI(), "Payment", "Buyer", 
													  QueryConfiguration.previousYear, QueryConfiguration.referenceDuration, true);
			/* [current year] */
			jsonString = qsCommon.getOrgCategoryStats(jsonString, graphStats, buyerUri.getURI(), "Payment", "Buyer", 
					  								  QueryConfiguration.currentYear, QueryConfiguration.referenceDuration, true);

			jsonString = jsonString.substring(0, jsonString.length() - 1) + "],";

			System.out.println("Querying Spending Items of: " + buyerUri.getURI());
			jsonString = qsBuyer.getBuyerSpendingItems(jsonString, graphEuFts, graphOrganizations, buyerUri.getURI());

			/** About **/
			jsonString = qsCommon.addAbout(jsonString, dateDataUpdated);

			/* close the 'normal' JSON */
			jsonString += "}]";

			/* close the 'couchDb' JSON */
			if (QueryConfiguration.couchDbUsage) {
				jsonString += "}";
			}

			hm.writeJsonFile(jsonString, "buyers", buyerUri.getURI());				
		}
		
		graphStats.close();
		graphEuFts.close();
		graphOrganizations.close();
		
		Logger.getInstance().write(logFilename, "Finished!\n");
	}
}