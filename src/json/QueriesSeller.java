package json;

import utils.TransliterationMethods;

import virtuoso.jena.driver.VirtGraph;
import virtuoso.jena.driver.VirtuosoQueryExecution;
import virtuoso.jena.driver.VirtuosoQueryExecutionFactory;

import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.Resource;

/**
 * @author G. Razis
 */
public class QueriesSeller {
	
	private HelperMethods hm = new HelperMethods();
	private TransliterationMethods trans = new TransliterationMethods();
	
	public String getSellerDetails(String jsonString, VirtGraph graphOrganizations, String sellerUri) {
		
		boolean resultsFlag = false;
		
		String queryOrg = "PREFIX vcard: <http://www.w3.org/2006/vcard/ns#> " +
		  		  		  "PREFIX gr: <http://purl.org/goodrelations/v1#> " +
		  		  		  "SELECT ?orgLegalName (MAX(?orgName) AS ?orgOtherName) ?afm ?validVatId ?addressRes ?pc " +
		  		  		  "?address ?city ?cn ?translation ?transliteration " +
		  		  		  "FROM <" + QueryConfiguration.queryGraphOrganizations + "> " +
		  		  		  "WHERE { " +
		  		  		  "OPTIONAL { " +
		  		  		  	"<" + sellerUri +"> gr:vatID ?afm . " +
		  		  		  "} . " +
		  		  		  "OPTIONAL { " +
		  		  		  	"<" + sellerUri +"> elod:validVatId ?validVatId . " +
		  		  		  "} . " + 
		  		  		  "OPTIONAL { " +
		  		  		  	"<" + sellerUri +"> gr:legalName ?orgLegalName . " +
		  		  		  "} . " +
			  		  	  "OPTIONAL { " +
			  		  	  	"<" + sellerUri +"> gr:name ?orgName. " +
		  		  		  "} . " +
		  		  		  "OPTIONAL { " +
			  		  		  "<" + sellerUri +"> vcard:hasAddress ?addressRes . " +
			  		  		  "?addressRes vcard:postal-code ?pc ; " +
						  		  		  "vcard:street-address ?address; " +
						  		  		  "vcard:locality ?city ; " +
						  		  		  "vcard:country-name ?cn . " +
		  		  		  "} . " +
		  		  		  "OPTIONAL { " +
		  		  		 	"<" + sellerUri +"> elod:translation ?translation . " +
		  		  		  "} . " +
		  		  		  "OPTIONAL { " +
		  		  		  	"<" + sellerUri +"> elod:transliterationLegalName ?transliteration . " +
		  		  		  "} . " +
		  		  		  "}";

		VirtuosoQueryExecution vqeOrg = VirtuosoQueryExecutionFactory.create(queryOrg, graphOrganizations);
		ResultSet resultsOrg = vqeOrg.execSelect();
		
		jsonString += "\"sellerDtls\": ";
		
		if (resultsOrg.hasNext()) {
			QuerySolution result = resultsOrg.nextSolution();
			Literal orgLegalName = result.getLiteral("orgLegalName");
			Literal orgOtherName = result.getLiteral("orgOtherName");
			Literal afm = result.getLiteral("afm");
			Resource addressRes = result.getResource("addressRes");
			Literal address = result.getLiteral("address");
			Literal pc = result.getLiteral("pc");
			Literal city = result.getLiteral("city");
			Literal translation = result.getLiteral("translation");
			Literal transliteration = result.getLiteral("transliteration");
			Literal validVatId = result.getLiteral("validVatId");
			boolean validVat = false;
			if (validVatId != null) {
				validVat = hm.validVatIdToBoolean(validVatId.getString());
			}
			
			if (orgLegalName != null) {
				jsonString += "{\"seller\": \"" + hm.cleanInvalidCharsJsonData(orgLegalName.getString().trim());
			} else if (orgOtherName != null) {
				jsonString += "{\"seller\": \"" + hm.cleanInvalidCharsJsonData(orgOtherName.getString().trim());
			} else {
				jsonString += "{\"seller\": \"" + "Το όνομα δεν έχει καταχωρηθεί";
			}
			
			String sellerEng = null; //Format: {transliterated // translated}
			String translationStr = null;
			String transliterationStr = null;
			
			if (transliteration != null) {
				transliterationStr = hm.cleanInvalidCharsJsonData(transliteration.getString()).trim();
			}
			if (translation != null) {
				translationStr = hm.cleanInvalidCharsJsonData(translation.getString()).trim();
			}
			if ( (transliterationStr == null) && (translationStr == null) ) { //create the transliteration now
				if (orgOtherName != null) {
					transliterationStr = trans.transliterationGenerator( hm.cleanInvalidCharsJsonData(orgOtherName.getString()).trim() );
				}
			}
			if ( (transliterationStr == null) && (translationStr == null) ) {
				sellerEng = "N/A";
			} else if (transliterationStr != null) {
				sellerEng = transliterationStr;
				if (translationStr != null) {
					sellerEng += " // " + translationStr;
				} //no need for 'else'
			} else if (translationStr != null) {
				sellerEng = translationStr;
			}
			
			jsonString += "\"," + "\"sellerEng\": \"" + sellerEng;
			
			if (validVat) {
				if (afm != null) {
					jsonString += "\"," + "\"vatId\": \"" + afm.getString() + 
							  	  "\"," + "\"vatIdEng\": \"" + afm.getString();
				}
			} else {
				if (afm != null) {
					jsonString += "\"," + "\"vatId\": \"" + "Μη έγκυρος ΑΦΜ" + 
								  "\"," + "\"vatIdEng\": \"" + "Invalid VAT ID";
				} else {
					jsonString += "\"," + "\"vatId\": \"" + "Ο ΑΦΜ δεν έχει καταχωρηθεί" + 
								  "\"," + "\"vatIdEng\": \"" + "N/A";
				}
			}
			
			if (validVat) {
				if (addressRes != null) {
					jsonString += "\"," + "\"address\": \"" + hm.cleanInvalidCharsJsonData(address.getString()) + 
								  "\"," + "\"addressEng\": \"" + trans.transliterationGenerator( hm.cleanInvalidCharsJsonData(address.getString()) ) + 
								  "\"," + "\"pc\": \"" + pc.getString() + "\"," + "\"pcEng\": \"" + pc.getString() + 
								  "\"," + "\"city\": \"" + hm.cleanInvalidCharsJsonData(city.getString().replace("\n", " ").replace("\r", " ")) + 
								  "\"," + "\"cityEng\": \"" + trans.transliterationGenerator( hm.cleanInvalidCharsJsonData(city.getString().
										  replace("\n", " ").replace("\r", " ")) ) + "\"},";
				} else {
					jsonString += "\"," + "\"address\": \"" + "Μη Διαθέσιμο" + "\"," + "\"addressEng\": \"" + "N/A" + 
								  "\"," + "\"pc\": \"" + "Μη Διαθέσιμο" + "\"," + "\"pcEng\": \"" + "N/A" + 
								  "\"," + "\"city\": \"" + "Μη Διαθέσιμο" + "\"," + "\"cityEng\": \"" + "N/A" + "\"},";
				}
			} else {
					jsonString += "\"," + "\"address\": \"" + "Μη Διαθέσιμο" + "\"," + "\"addressEng\": \"" + "N/A" + 
							  	  "\"," + "\"pc\": \"" + "Μη Διαθέσιμο" + "\"," + "\"pcEng\": \"" + "N/A" + 
							  	  "\"," + "\"city\": \"" + "Μη Διαθέσιμο" + "\"," + "\"cityEng\": \"" + "N/A" + "\"},";
			}
			
			resultsFlag = true;
		} else {		//not found in organizations graph -> look at EuFts graph for address
			String queryOrg2 = "PREFIX elod: <http://linkedeconomy.org/ontology#> " +
					"PREFIX gr: <http://purl.org/goodrelations/v1#> " +
					"PREFIX vcard: <http://www.w3.org/2006/vcard/ns#> " +
					"SELECT DISTINCT ?sellerName ?streetAddress ?city ?postalCode ?country " +
					"FROM <" + QueryConfiguration.queryGraphEuFts + "> " +
					"WHERE { " +
					"<" + sellerUri +"> vcard:hasAddress ?sellerAddress . " +
					"OPTIONAL { " +
						"<" + sellerUri +"> gr:name ?sellerName . " +
					"} . " +	
					"OPTIONAL { " +
						"?sellerAddress vcard:street-address ?streetAddress . " +
					"} . " +
					"OPTIONAL { " +
						"?sellerAddress vcard:locality ?city . " +
					"} . " +
					"OPTIONAL { " +
						"?sellerAddress vcard:postal-code ?postalCode . " +
					"} . " +
					"OPTIONAL { " +
						"?sellerAddress vcard:country ?country . " +
					"} . " +
					"}";
			
			VirtuosoQueryExecution vqeOrg2 = VirtuosoQueryExecutionFactory.create(queryOrg2, graphOrganizations);
			ResultSet resultsOrg2 = vqeOrg2.execSelect();
			
			if (resultsOrg2.hasNext()) {
				QuerySolution result = resultsOrg2.nextSolution();
				Literal sellerName = result.getLiteral("sellerName");
				Literal address = result.getLiteral("streetAddress");
				Literal city = result.getLiteral("city");
				Literal postalCode = result.getLiteral("postalCode");
				Literal country = result.getLiteral("country");
				
				if(sellerName != null) {
					jsonString += "{\"sellerId\": \"" + sellerName.getString().trim();
				}
				else {
					jsonString += "{\"sellerId\": \"" + "-";
				}
				
				if(address != null) {
					jsonString += "\"," + "\"address\": \"" + address.getString().trim();
				}
				else {
					jsonString += "\"," + "\"address\": \"" + "-";
				}
				
				if(city != null) {
					jsonString += "\"," + "\"city\": \"" + city.getString().trim();
				}
				else {
					jsonString += "\"," + "\"city\": \"" + "-";
				}
				
				if(postalCode != null) {
					jsonString += "\"," + "\"postalCode\": \"" + postalCode.getString().trim();
				}
				else {
					jsonString += "\"," + "\"postalCode\": \"" + "-";
				}
				
				if(country != null) {
					jsonString += "\"," + "\"country\": \"" + country.getString().trim() + "\"},";
				}
				else {
					jsonString += "\"," + "\"country\": \"" + "-" + "\"},";
				}
				
				vqeOrg2.close();
				resultsFlag = true;
			}
		}
		
		vqeOrg.close();
		
		if (!resultsFlag) {
			jsonString += "{},";
		}
		
		return jsonString;
	}
	
	public String getSellerSpendingItems(String jsonString, VirtGraph graphEuFts, VirtGraph graphOrganizations, String sellerUri) {
		
		boolean resultsFlag = false;
		
		String queryPaymentsItems = "PREFIX elod: <http://linkedeconomy.org/ontology#> " +
									"PREFIX dcτerms: <http://purl.org/dc/terms/> " +
									"PREFIX gr: <http://purl.org/goodrelations/v1#> " +
									"SELECT DISTINCT ?buyer ?date ?contractId ?buyerLegalName (SAMPLE (?sellerName) AS ?sellerName) (SAMPLE (?legalName) AS ?legalName) xsd:decimal(?amount) AS ?paymentAmount xsd:decimal(?totalAmount) AS ?totalAmount ?description " + 
									"FROM <" + QueryConfiguration.queryGraphEuFts + "> " +
									"FROM <" + QueryConfiguration.queryGraphOrganizations + "> " +
									"WHERE { " +
									"?spendingItem elod:hasExpenditureLine ?expLine ; " +
												  "elod:hasRelatedContract ?contract . " +
									"?contract elod:contractId ?contractId ; " +
											  "pc:item ?offering ; " +
											  "elod:buyer ?buyer ; " +
											  "dcτerms:issued ?date ; " +
											  "pc:actualPrice ?currentAmount ; " +
											  "rdf:type pc:Contract . " +	
									"OPTIONAL { " +
										"<" + sellerUri + "> gr:name ?sellerName . " +
									"} " +
									"OPTIONAL { " +
										"<" + sellerUri + "> gr:legalName ?legalName . " +
									"} " +
									"?expLine elod:amount ?ups ; " +
											 "elod:seller <" + sellerUri + "> . " +
									"?ups gr:hasCurrencyValue ?amount . " +
									"?buyer gr:legalName ?buyerLegalName . " +
								    "?offering gr:includesObject ?typeAndQuantityNode . " +
									"?typeAndQuantityNode gr:typeOfGood ?typeOfGood . " +
									"?typeOfGood gr:description ?description. " +
									"?currentAmount gr:hasCurrencyValue ?totalAmount . " +
									"FILTER (?date >= \"" + "2007" + "\"^^xsd:gYear) . " +
									"FILTER (?date <= \"" + String.valueOf(Integer.valueOf(QueryConfiguration.currentYear)) + "\"^^xsd:gYear) . " +
									"}" ;

		VirtuosoQueryExecution vqePaymentsItems = VirtuosoQueryExecutionFactory.create(queryPaymentsItems, graphEuFts);
		ResultSet resultsPaymentsItems = vqePaymentsItems.execSelect();		
		
		jsonString += "\"sellerSpendingItems\": [";
		
		while (resultsPaymentsItems.hasNext()) {
			QuerySolution result = resultsPaymentsItems.nextSolution();
			String sellerName = null, sellerPart = null, description = null;
			String date = result.getLiteral("date").getString();
			String contractId = result.getLiteral("contractId").getString();
			String buyerLegalName = result.getLiteral("buyerLegalName").getString();
			String buyerPart = "<a href=\\\"" + hm.urlProfilePage(result.getResource("buyer").toString(), "Buyer") + 
							   "\\\" target=\\\"_blank\\\">" + hm.cleanInvalidCharsJsonData(buyerLegalName) + "</a>";
			
			if (result.getLiteral("legalName") != null) {
				sellerName = result.getLiteral("legalName").getString();
				sellerPart = "<a href=\\\"" + hm.urlProfilePage(sellerUri, "Seller") + 
							 "\\\" target=\\\"_blank\\\">" + hm.cleanInvalidCharsJsonData(sellerName) + "</a>";
			} else if (result.getLiteral("sellerName") != null){
				sellerName = result.getLiteral("sellerName").getString();
				sellerPart = "<a href=\\\"" + hm.urlProfilePage(sellerUri, "Seller") + 
							 "\\\" target=\\\"_blank\\\">" + hm.cleanInvalidCharsJsonData(sellerName) + "</a>";
			} else {
				sellerPart = "<a href=\\\"" + hm.urlProfilePage(sellerUri, "Seller") + 
						 	 "\\\" target=\\\"_blank\\\">" + "N/A" + "</a>";
			}
			
			String paymentAmount = hm.roundAmountNoDecimals(result.getLiteral("paymentAmount"));
			String totalAmount = hm.roundAmountNoDecimals(result.getLiteral("totalAmount"));
			Literal descriptionLiteral = result.getLiteral("description");
			
			if (descriptionLiteral != null) {
				description = descriptionLiteral.getString().replace("\"", "");
			} else {
				description = "-";
			}
			
			jsonString += "{\"date\": \"" + date.split("-")[0] + "\"," + "\"contractId\": \"" + contractId + 
						  "\"," + "\"buyerLegalName\": \"" + buyerPart + "\"," + "\"sellerName\": \"" + 
						  sellerPart + "\"," + "\"paymentAmount\": \"" + paymentAmount + "\"," + 
						  "\"totalAmount\": \"" + totalAmount + "\"," + "\"description\": \"" + description;

			jsonString += "\"},";
			
			resultsFlag = true;
		}
		
		vqePaymentsItems.close();
		
		if (resultsFlag) {
			jsonString = jsonString.substring(0, jsonString.length() - 1) + "],";
		} else {
			jsonString = jsonString.substring(0, jsonString.length()) + "],";
		}
		
		return jsonString;
	}
	
}